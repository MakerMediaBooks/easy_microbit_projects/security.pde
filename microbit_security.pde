import processing.serial.*;

Serial myPort;  
int val;      

void setup() 
{
  size(800, 800);
  for (int i = 0; i < 2; i++){
    println(Serial.list()[i]);}
  String portName = Serial.list()[1];
  
  myPort = new Serial(this, portName, 115200);
}

void draw()
{
  if ( myPort.available() > 0) {
    val = myPort.read();     
    println(val);
  }
  background(255);             
  if (val > 48) {              
    background(255, 0, 0);}                  
  else {                       
    background(0, 255, 0);}                 
}